(function() {
	zip = (...rows) => [...rows[0]].map((_,c) => rows.map(row => row[c]))

	function getInput(){
		if (window.location.hostname.includes("acm.timus.ru"))
			return document.querySelectorAll('.sample td:first-child pre');
		else
		if (window.location.hostname.includes("acmp.ru"))
			return document.querySelectorAll('.main td:first-child + td');
		else
		if (window.location.hostname.includes("codeforces."))
			return document.querySelectorAll('.input pre');
		else {
			console.log("unknown site");
			return [];
		}
	}

	function getOutput(){
		if (window.location.hostname.includes("acm.timus.ru"))
			return document.querySelectorAll('.sample td:last-child pre');
		else
		if (window.location.hostname.includes("acmp.ru"))
			return document.querySelectorAll('.main td:last-child');
		else
		if (window.location.hostname.includes("codeforces."))
			return document.querySelectorAll('.output pre');
		else {
			console.log("unknown site");
			return [];
		}
	}

	function getTests(){
		return zip(getInput(), getOutput()).map(a=>a.map(v=>JSON.stringify(v.innerText.trim())));
	}

	function getTestsString(){
		return "\tTESTIO(" + getTests().join(");\n\tTESTIO(") + ");";
	}

	if(getInput().length && getOutput().length) {
		const body = document.querySelector('body');

		const button = document.createElement('img');

		button.classList.add('acm-gearbox-copy-button');
		button.setAttribute('src', browser.runtime.getURL('icons/clipboard.svg'))
		button.setAttribute('alt', 'Copy!')

		button.onclick = function(){
			navigator.clipboard.writeText(getTestsString()).then(()=>console.log('copied'), err=>console.log('err:', err));
		}

		const style = document.createElement('link');
		style.type = 'text/css';
		style.rel = 'stylesheet';
		style.href = browser.runtime.getURL('button.css');

		body.append(style);
		body.append(button);
	}
})()