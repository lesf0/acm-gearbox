TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -Dnoacm -iquote/usr/include/bsd/
QMAKE_LFLAGS += -lbsd

SOURCES += \
        ../../main.cpp
