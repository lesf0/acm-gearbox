// Copyright 2005, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#define noacm
#define main one_potato();void two_potato
#include "../main.cpp"
#undef main
#undef TEST
#undef TESTIO

#include "gtest/gtest.h"

TEST(types, int){
	EXPECT_EQ(sizeof(u8), 1);
	EXPECT_EQ(sizeof(i8), 1);
	EXPECT_EQ(sizeof(u16), 2);
	EXPECT_EQ(sizeof(i16), 2);
	EXPECT_EQ(sizeof(u32), 4);
	EXPECT_EQ(sizeof(i32), 4);
	EXPECT_EQ(sizeof(u64), 8);
	EXPECT_EQ(sizeof(i64), 8);
}

TEST(types, cbuf){
	EXPECT_EQ(sizeof(cbuf<42>), 42);
	EXPECT_EQ(sizeof(decltype(declval<cbuf<42>>()[0])), 1);
}

TEST(types, bool){
	EXPECT_EQ((is_same<Bool, bool>::value), false);
	EXPECT_EQ(True, true);
	EXPECT_EQ(False, false);
}

TEST(constants, exponential){
	EXPECT_EQ(E2, 100);
	EXPECT_EQ(E3, 1000);
	EXPECT_EQ(E4, 10000);
	EXPECT_EQ(E5, 100000);
	EXPECT_EQ(E6, 1000000);
	EXPECT_EQ(E7, 10000000);
	EXPECT_EQ(E8, 100000000);
	EXPECT_EQ(E9, 1000000000);
}

TEST(feature, spr){
	auto s=spr((i8)-1, (u8)2, (i16)-3, (u16)4, (i32)-5, (u32)6, (i64)-7, (u64)8, (int)9, (long)10, (long long)11, (short)12, (f32)M_PI, (f64)M_PI, (f80)M_PI, '@', true, "foobar", (u8*)0+0x1234);
	EXPECT_EQ(s, "-1 2 -3 4 -5 6 -7 8 9 10 11 12 3.141593 3.141593 3.141593 @ 1 foobar 0x1234");
}

TEST(feature, for){
	u32 c;

	c=0;
	forn(i,42)c++;
	EXPECT_EQ(c, 42);

	c=0;
	for1n(i,42)c++;
	EXPECT_EQ(c, 41);

	c=0;
	fornd(i,42)c++;
	EXPECT_EQ(c, 42);

	c=0;
	forab(i,1,10)c++;
	EXPECT_EQ(c, 10);

	c=0;
	repn(42)c++;
	EXPECT_EQ(c, 42);
}

TEST(feature, talloc){
	u32 n=rrand(10, 20);
	talloc(a, n, pair<int, int>);
	a[n-1] = {0, 1};
	EXPECT_EQ(a[n-1], make_pair(0, 1));
}

TEST(feature, qsort){
	pair<u32, u32> arr[50];

	forn(i, 50) arr[i] = {0, rrand(E4, E5)};

	sqsort(arr, 50);
	for1n(i, 50) EXPECT_LE(arr[i-1], arr[i]);

	fqsort(arr, 50, [](auto* a, auto* b){return (*a<*b) ? 1 : -1;});
	for1n(i, 50) EXPECT_GE(arr[i-1], arr[i]);
}

TEST(feature, bsearch){
	int arr[]{10, 20, 30, 40, 50};

	forab(i, 1, 50) {
		auto r = sbsearch(i, arr, 5);

		if(i % 10) {
			EXPECT_EQ(r, (void*)NULL);
		}else{
			EXPECT_EQ(r, arr + i/10 - 1);
		}
	}

	forab(i, 11, 50) {
		auto r = fbsearch(i, arr+1, 4, [](auto* a, auto* b){
			if(*a > *b){
				return 1;
			}else{
				return -1+(*a > *(b-1));
			}
		});

		EXPECT_EQ(*r, (i+9) / 10 * 10);
	}
}

TEST(feature, gcd){
	EXPECT_EQ(gcd(2*2*3,2*3*3), 2*3);
}

TEST(feature, lcm){
	EXPECT_EQ(lcm(2*2*3,2*3*3), 2*2*3*3);
}

TEST(inputgen, string) {
	::Test::InputGen g("foo");
	EXPECT_EQ(g(), "foo");
	EXPECT_EQ(g(), "");
}

TEST(inputgen, number) {
	::Test::InputGen g(42);
	EXPECT_EQ(g(), "42");
	EXPECT_EQ(g(), "");
}

TEST(inputgen, function) {
	::Test::InputGen g(bind(rrand<u32>, E4, E5-1));
	EXPECT_EQ(g().length(), 5);
	EXPECT_EQ(g().length(), 5);
}

TEST(inputgen, prod) {
	::Test::Prod<4, '.'> g{[](){return 42;}};
	EXPECT_EQ(g(), "42.42.42.42");
}

TEST(inputgen, rep) {
	::Test::Rep g(2, 42);
	EXPECT_EQ(g(), "42");
	EXPECT_EQ(g(), "42");
	EXPECT_EQ(g(), "");
}

TEST(inputgen, range) {
	::Test::Range g(2, 3187, -5);
	EXPECT_EQ(g(), "3187");
	EXPECT_EQ(g(), "3182");
	EXPECT_EQ(g(), "");
}

TEST(inputgen, iterator) {
	::Test::InputGenIter i1{"1 2 3 4 5"};
	::Test::InputGenIter i2{::Test::Range(5)};
	EXPECT_EQ(equal(i1, {}, i2, {}), true);
}

TEST(checker, inputgen) {
	::Test::Checker c{"1 2 3 4 5"};
	EXPECT_EQ(c("5\n4 1 3 5 2", "1 2 3 4 5"), true);
	EXPECT_EQ(c("5\n4 1 3 5 2", "5 4 3 2 1"), false);
}

TEST(checker, voidfn) {
	::Test::Checker c{[](){
		scna(a, n, int);
		sqsort(a, n);
		prn(a, n);
	}};
	EXPECT_EQ(c("5\n4 1 3 5 2", "1 2 3 4 5"), true);
	EXPECT_EQ(c("5\n4 1 3 5 2", "5 4 3 2 1"), false);
}

TEST(checker, boolfn) {
	::Test::Checker c{[](){
		scna(a, n, int);
		for1n(i, n){
			if(a[i-1] > a[i]) return False;
		}
		return True;
	}};
	EXPECT_EQ(c("5\n4 1 3 5 2", "1 2 3 4 5"), true);
	EXPECT_EQ(c("5\n4 1 3 5 2", "5 4 3 2 1"), false);
}
