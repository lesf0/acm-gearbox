## Compillation

Build it on any modern c++ compiler (c++14 at least).

`-Dnoacm` flag to enable debug stuff (highly recommended, on Windows works only under Cygwin and WSL)

`-iquote/usr/include/bsd/ -lbsd` flags to compile under Linux in debug mode (for `fropen` / `fwopen` support)

## Usage

`i[0-9]+`, `u[0-9]+`, `f[0-9]+` - shorthand for integer and float types.

`cbuf<size_t N>` - alias for `char[N]` to use in macros.

`pr`, `sc` - `printf` and `scanf` shorthands with format and references generated automatically. `pr(42)` equals `printf("%d\n", 42)`. `int a; sc(a)` equals `int a;scanf("%d",&a)`. `pr` accepts optional template parameters - multichar constants for format, line ending and separator. `prs` is an equivalent to `pr<0,' '>`. `pr<'.2'>(M_PI)` to print `3.14`.

`sca` - allocate and read. `sca(u32,a,b)` equals `u32 a,b; sc(a,b)`

`scn`, `scnn`, `scna` - read `n` args. `scn(a,n)` reads `n` entries to existing array `a`. `scnn(a,n,type)` also allocates array `a` of size `n`. `scna(a,n,type)` also allocates `n` and reads it.

`prn` prints `n` elements of array, space-separated, ending with newline.
`prr` prints elements of any iterable container in the same way.

`dbug`, `dbugn` - like `pr` and `prn`, but only called if compiled locally. Useful for debugging.

`talloc` allocates `n` elements of a given type, stack-allocation if supported, heap-allocation otherwise.

`forn`, `for1n`, `fornd`, `forab`, `repn`, `repsc` are pretty much self-explainatory `for` bindings.

`E3`..`E9` - constant values of `10**3`..`10**9`

`sqsort`, `tqsort` and `fqsort` - `qsort` bindings with auto-comparator, auto-comparator for given type and given lambda comparator.

`sbsearch`, `tbsearch` and `fbsearch` - simillar `bsearch` bindings.

`memnul`, `memneg` - bindings to `malloc` with `0` and `-1`.

`rrand` - uniform distribution random number generator on `[a,b]` range.

`acm_in`, `acm_out` - if `acm_files` defined, filenames used on a checker server.

## Testing

Local test cases can be specified using `TEST` macro for input-only test or `TESTIO` macro for input-output test. Any number of tests supported.

### Args:

`TEST` macro accepts list of `std::function<string()>` generators, each yielding string portions until it ends (or until no one is reading).

String literals and arithmetic constants are automatically converted to at-most-once-yielding function.

Functions yielding an arithmetic type are automatically converted to function yielding a string.

`TESTIO` macro accepts two args. First one is a list same as `TEST` accepts except it has to be brace-enclosed if there's more than one generator. Second is a `std::function<bool(std::string, std::string)>` checker, which accepts generated input and generated output as `std::string`s and returns `true` if output was correct.

Input generators are implicitly convertible to checkers. This way program will check if space-separated tokens in output matches space-separated tokens in generator. Leftover tokens in generator are considered to be errors.

`std::function<void()>` is implicitly convertible to checker. That function would recieve input on `stdin` and then its `stdout` tokens would be matched against tokens in program output. Could be useful if there's 100% working but ineffecient solution.

`std::function<Bool()>` is implicitly convertible to checker as well. That function would recieve program output on `stdin` and should return `True` if it's correct and `False` otherwise. `Bool` instead of `bool` is used to avoid ambiguity with input generator ctor from arithmetic types.

### Pre-defined helpers:
* `Rep(n, f)` - repeat generator `n` times.
* `Prod<N=1, S=' '>{f...}` - decartian product of generators. Accepts number of repititions and separator in its template args
* `Range(n, a=1, d=1)` - integer range generator.

### Examples:

```
TEST("5\n1 2 3 4 5")
```

```
TEST(
	E5, E5,
	Prod{
		Range{E5-1},
		Range{E5-1, 2},
		Range{E5-1, 10, 10}
	},
	E5, 1, E6
);
```

```
TEST(
	Rep(38, "I will not waste chalk,")
)
```

```
TEST(
	E5,
	[](){return rrand(1,100);}
);
```

```
//	Note: this is an infinite generator
TEST(
	Prod<10, 0>{
		bind(rrand<char>,'a','z')
	}
);
```

```
TESTIO(
	"This string should be reversed!",
	"!desrever ed dluohs gnirts sihT"
);
```

```
TESTIO(
	//	this string should be shuffled
	Prod<100, 0>{
		bind(rrand<char>,'a','z')
	},
	[](string i, string o){
		i.erase(find_if(i.begin(), i.end(), [](int ch) {return isspace(ch);}), i.end());
		o.erase(find_if(o.begin(), o.end(), [](int ch) {return isspace(ch);}), o.end());
		if(i==o){
			return false;
		}
		sqsort(i.data(), i.length());
		sqsort(o.data(), o.length());
		return i==o;
	}
);
```

```
TESTIO(
	//	this sequence should be sorted
	{
		E5,
		bind(rrand<i32>, -E9, E9)
	},
	[](){
		u32 c, p = INT32_MIN;
		while(sc(c) > 0){
			if(c < p){
				return False;
			}
			p = c;
		}
		return True;
	}
);
```

```
TESTIO(
	bind(rrand<i32>, -E9, E9),
	[](){
		sca(i32, a, b);
		
		//	ineffecient but working algo:
		i32 ans;
		do {
			ans = rrand(-2*E9, 2*E9);
		} while(ans - a != b);

		pr(ans);
	}
);
```

## TODO

* Windows support (mingw, msvc)
* musl support
* Interactive test
* Project files for various popular IDE's
* Examples
